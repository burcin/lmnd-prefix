#!/bin/bash
set -e


PREFIX=prefix-overlay/master
GX86=gx86/master

die() { echo "* $*"; exit 1; }

# check input
[ $# -eq 1 ] || die "Usage: `basename $0` category/package"
# argument should be of the form cat-egory/package_name
[[ "${1}"  == [A-Za-z-]*/[A-Za-z-0-9_]* ]] || \
	die "${1} does not look like a valid argument"

CAT=${1%\/*}
PACKAGE=${1#*\/}
# category is in profiles/categories
grep -q ${CAT} profiles/categories || \
	die "category ${CAT} is not available in lmonade"

# check if environment variables needed for repoman are defined
: ${DISTDIR:="$HOME"/lmnd/distfiles}
[ -d "${DISTDIR}" ] || die "DISTDIR should point to an existing directory"


if [ -n "$(git ls-tree --name-only -r --full-tree $PREFIX ${1})" ]; then
    # if it exists in prefix-overlay
    echo "found ${1} in prefix"
    git checkout "$PREFIX" -- "${1}"
elif [ -n "$(git ls-tree --name-only -r --full-tree $GX86 ${1})" ]; then
    # if it exists in gx86
    echo "found ${1} in gx86"
    git checkout "$GX86" -- "${1}"
else
    die "package ${1} not found in prefix or gx86"
fi


# prepare environment, go to the directory and "repoman fix"
export DISTDIR
export FETCHCOMMAND="wget -t 3 -T 60 --no-check-certificate --passive-ftp -O \"\${DISTDIR}/\${FILE}\" \"\${URI}\""
export RESUMECOMMAND="wget -c -t 3 -T 60 --no-check-certificate --passive-ftp -O \"\${DISTDIR}/\${FILE}\" \"\${URI}\""
export FEATURES="assume-digests"

pushd "${1}"
set +e # repoman is fussy
repoman fix
set -e
popd

# update package.keywords
scripts/sync/update_package_keywords.sh
