# lmonade-prefix portage repository

[lmonade](http://www.lmona.de) is a light-weight meta distribution of
scientific software that can be installed without administrative rights on
Unix based operating systems. It creates a uniform environment for
development to simplify code sharing and distribution especially when
complex dependencies are involved.  This platform enables researchers to
build on existing tools without fear of losing users to baffling
installation instructions.


## Installation instructions

To set up a development environment based on this repository, follow [the instructions on the wiki](http://wiki.lmona.de/devel/workflow).

## Contact

 - [web site](http://www.lmona.de): http://www.lmona.de
 - [Google group](https://groups.google.com/forum/?fromgroups#!forum/lmnd-devel): lmnd-devel@googlegroups.com
 - [IRC](http://webchat.freenode.net?channels=lmnd&uio=MT11bmRlZmluZWQb1):
   #lmnd on freenode.net
